namespace :cron do
  task :device_sync => :environment do
    Rails.logger.info "device sync start"
    Device.sync
    Rails.logger.info "device sync start"
  end

  task :check_unreport_device => :environment do
    ts       = Time.new.hour*12+Time.new.min/5
    redis    = Redis.new
    reported = redis.smembers "devices_#{ts}"
    total    = Device.where(rtx:"ON").map(&:hostname).to_a
    if total.size > reported.size
      nodate = total-reported
      Rtx.send_notify("[#{nodate.join(",")}] no data, please check! ")
    end
  end
end
