namespace :rrd do
  desc "draw device groups graph."
  task :draw_device_group => :environment do
    DeviceGroup.all().each do |group|
      hit_rate_less_70=0
      hit_rate_less_90=0
      hit_rate_more_90=0
      response_time_less_2=0
      response_time_less_5=0
      response_time_more_5=0

      group.devices.each do |device|
        rrd_datas = device.rrd_fetch()
        if rrd_datas.size > 0 and rrd_datas[1]!= "-nan"
          hit_rate = ("%.3f" % rrd_datas[1]).to_f
          response_time = ("%.3f" % rrd_datas[3]).to_f
          if  hit_rate< 70
            hit_rate_less_70+=1
          elsif hit_rate < 90
            hit_rate_less_90+=1
          else
            hit_rate_more_90+=1
          end
          if response_time<0.002
            response_time_less_2+=1
          elsif response_time<0.005
            response_time_less_5+=1
          else
            response_time_more_5+=1
          end
        end
      end

      Note.update_group_rrd("#{ENV['rrd_file_path']}#{group.name}.rrd",[hit_rate_less_70,hit_rate_less_90,hit_rate_more_90,response_time_less_2,response_time_less_5,response_time_more_5])
      Note.graph_group(group.name,"hr",["hrl70","hrl90","hrm90"],["<70","<90",">=90"],"number of devices")
      Note.graph_group(group.name,"rt",["rtl2","rtl5","rtm5"],["<2","<5",">=5"],"number of devices")
    end
  end
end