class DevicesController < ApplicationController
  def index
    @devices = Device.all
  end

  def edit
    @device = Device.find(params[:id])
  end

  def update
    @device = Device.find(params[:id])

    respond_to do |format|
      if @device.update_attributes(params[:device])
        format.html { redirect_to devices_path}
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end

  def new
    @device = Device.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @device}
    end
  end

  def create
    @device = Device.new(params[:device])
    respond_to do |format|
      if @device.save
        format.html { redirect_to devices_path, notice: 'Fiverror was successfully created.' }
        format.json { render json: @device, status: :created, location: @fiverror }
      else
        format.html { render action: "new" }
        format.json { render json: @device.errors, status: :unprocessable_entity }
      end
    end
  end
end
