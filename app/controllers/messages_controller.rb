#encoding: utf-8
class MessagesController < ApplicationController
  skip_before_filter :verify_authenticity_token

  def index
    @messages = Message.all 
    @keys = Redis.new.keys('ip-*')
  end

  # POST /messages
  # POST /messages.json
  def create
    message = request.raw_post.force_encoding("UTF-8")
    PygmentsWorker.perform_async(message)
    render :json => 'ok'
  end

  # POST /notes
  # POST /notes.json
  def note
    note = request.body.read
    if not note.empty?
      RtWorker.perform_async(note)
      NotesWorker.perform_async(note)
    end
    render :json => 'ok'
  end

  def status
    data = request.body.read
    $status_log.info data
    render :json => 'ok'
  end
end
