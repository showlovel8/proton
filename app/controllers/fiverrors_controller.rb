class FiverrorsController < ApplicationController
  # GET /fiverrors
  # GET /fiverrors.json
  def index
    @fiverrors = Fiverror.order("count desc").order("created_at desc")

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @fiverrors }
    end
  end

  # GET /fiverrors/1
  # GET /fiverrors/1.json
  def show
    @fiverror = Fiverror.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @fiverror }
    end
  end

  # GET /fiverrors/new
  # GET /fiverrors/new.json
  def new
    @fiverror = Fiverror.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @fiverror }
    end
  end

  # GET /fiverrors/1/edit
  def edit
    @fiverror = Fiverror.find(params[:id])
  end

  # POST /fiverrors
  # POST /fiverrors.json
  def create
    @fiverror = Fiverror.new(params[:fiverror])
    @fiverror.area = Message.getisp(@fiverror.ip)
    respond_to do |format|
      if @fiverror.save
        format.html { redirect_to @fiverror, notice: 'Fiverror was successfully created.' }
        format.json { render json: @fiverror, status: :created, location: @fiverror }
      else
        format.html { render action: "new" }
        format.json { render json: @fiverror.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /fiverrors/1
  # PUT /fiverrors/1.json
  def update
    @fiverror = Fiverror.find(params[:id])

    respond_to do |format|
      if @fiverror.update_attributes(params[:fiverror])
        format.html { redirect_to @fiverror, notice: 'Fiverror was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @fiverror.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fiverrors/1
  # DELETE /fiverrors/1.json
  def destroy
    @fiverror = Fiverror.find(params[:id])
    @fiverror.destroy

    respond_to do |format|
      format.html { redirect_to fiverrors_url }
      format.json { head :no_content }
    end
  end
end
