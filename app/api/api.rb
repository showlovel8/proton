module Proton
  class API < Grape::API

    resource do

      desc "get alert count between time"
      params do
        optional :stime, type: String, desc:"start time"
        optional :etime, type: String, desc:"end   time"
      end
      get :alerts do
        stime = params[:stime]
        etime = params[:etime]
        Device.get_counts(stime, etime) 
      end

      desc "get alerts between time"
      params do
        optional :stime, type: String, desc:"start time"
        optional :etime, type: String, desc:"end   time"
      end
      get :alerts_info do
        stime = params[:stime]
        etime = params[:etime]
        Alert.get_one_day_info(stime, etime)
      end

      desc "response for github hooks"
      post :githook do
        git_info = JSON.parse(params[:payload])
        cmd      = "cd /data/github/test; pwd; git pull"
        system cmd
        cmd      = "cd /data/github/master; pwd; git pull"
        system cmd
      end
    end
  end
end
