#encoding: utf-8
module ApplicationHelper
  require 'ipaddr'
  def init_ip_hash
    ip_hash = {}
    location_hash = {}
    ip_array = []
    begin
      file = File.new("public/chunzhen_ip", "r")
      while (line = file.gets)
        line = line.encode!('UTF-8', 'GBK', :invalid => :replace)
        arr = line.split(" ")
        begin
          ip_hash[arr[2].to_s+arr[3].to_s] = [IPAddr.new(arr[0]).to_i,IPAddr.new(arr[1]).to_i]
          if location_hash.has_key?(IPAddr.new(arr[0]).to_i)
            puts "duplicate key #{line} with #{location_hash[IPAddr.new(arr[0]).to_i]}"
          else
            location_hash[IPAddr.new(arr[0]).to_i] = arr[2].to_s+arr[3].to_s
            ip_array << IPAddr.new(arr[0]).to_i
          end
        rescue => err
          puts "Ip: #{arr[0]}-#{arr[1]}"
          puts "Exception: #{err}"
        end
      end
      file.close
    rescue => err
      puts "Exception: #{err}"
    end
    return ip_hash,ip_array.sort,location_hash
  end

  def search(ip_hash,ip)
    ip = IPAddr.new(ip).to_i
    ip_hash.each do |key,value|
      if (value[0]..value[1])===ip
        return key
      end
    end
    return "lookup fail"
  end

  def bisect_left(list, item, lo = 0, hi = list.size)
    if block_given?
      while lo < hi
        i = (lo + hi - 1) >> 1

        if 0 <= yield(list[i], item)
          hi = i
        else
          lo = i + 1
        end
      end
    else
      while lo < hi
        i = (lo + hi - 1) >> 1

        if 0 <= (list[i] <=> item)
          hi = i
        else
          lo = i + 1
        end
      end
    end
    if list[hi]==item
      return hi
    else
        return hi >0 ? hi-1:0
    end
  end

end
