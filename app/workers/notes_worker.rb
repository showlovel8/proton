class NotesWorker
  include Sidekiq::Worker
  # sidekiq_options queue: "high"
  # sidekiq_options retry: false

  def perform(note)
    notes = JSON.parse(note)
    rrd_filename = "#{ENV['rrd_file_path']}#{notes['hostname']}.rrd"
    if not File.exists?(rrd_filename)
      Note.create_rrd(rrd_filename)
    end

    Note.update_rrd(rrd_filename,notes)
    Note.graph(notes['hostname'],"rt",1000,"milliseconds","Response Time",3,nil)
    Note.graph(notes['hostname'],"hr",1,"percent(%)","Hit Rate",2,nil)
    Note.graph(notes['hostname'],"os",1,"total cached objects","Cached Objects",0,nil)
    Note.graph(notes['hostname'],"rs",1,"requests per 5 minutes","HTTP Requests",0,nil)
    Note.graph(notes['hostname'],"co",1,"percent(%)","CPU Usage",2,nil)
    Note.graph_bps(notes['hostname'],"vs",1000,"Bytes Per Second","Mean Velocity",2,nil)
  end
  
end
