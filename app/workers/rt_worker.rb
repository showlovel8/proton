class RtWorker
  include Sidekiq::Worker
  sidekiq_options queue: "high"
  # sidekiq_options retry: false    

  def perform(note)
    ts    = Time.new.hour*12+(Time.new.min+1)/5
    redis = Redis.new

    notes = JSON.parse(note)
    redis.sadd "devices_#{ts}", notes['hostname']
    redis.expire "devices_#{ts}", 60*10

    logger.info "--noteinfo #{notes}--"
    if notes['response_time'].to_f>0.01  
      device = Device.where(["binary hostname=?", notes['hostname']]).first
      if device
        device.alerts.create(ms:notes['response_time'])
      else
        Device.create(hostname:notes['hostname']).alerts.create(ms:notes['response_time'])
      end
      redis = Redis.new
      if redis
        redis.incr(notes['hostname'])
        redis.expire(notes['hostname'],900)
        overtimes = redis.get(notes['hostname'])
        if overtimes.to_i > 3
          redis.del(notes['hostname'])
          if Time.now.hour > 6 
            dev = Device.where(["binary hostname=?", notes['hostname']]).first
            Rtx.send_notify("http://sispub.ssr.chinacache.com/devstatus/getngurlbyhostname/?hostname=#{notes['hostname']} response time is #{notes['response_time']}. already #{overtimes} times. #{dev.try(:description)}")
          end
        end
      end
    end  
  end

end
