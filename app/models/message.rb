require 'redis'
class Message < ActiveRecord::Base
  attr_accessible :request_ip, :update_time

  def self.getisp(ip)
    url = "http://www.youdao.com/smartresult-xml/search.s?encoding=utf8&type=ip&q="
    uri = URI.parse(url+ip)
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    raw_info = response.body
    # logger.info(raw_info.force_encoding('utf-8'))
    doc = Nokogiri::XML(raw_info.force_encoding('UTF-8'))
    return (doc.xpath("//smartresult/product/location").text)
  end

  # def self.getip(hostname)
  #   url = "http://rcmsapi.chinacache.com:36000/device/"
  #   uri = URI.parse(url+hostname)
  #   http = Net::HTTP.new(uri.host, uri.port)
  #   request = Net::HTTP::Get.new(uri.request_uri)
  #   response = http.request(request)
  #   raw_info = response.body
  #   jsonArray = JSON.parse(raw_info)
  #   p jsonArray
  #   return jsonArray
  # end

  def self.update_time(request_ip)
    redis = Redis.new
    if redis
      redis.set("ip-"+request_ip,Time.now)
    end
  end

  def self.warning_time(request_ip)
    redis = Redis.new
    if redis
      redis.set("ip-"+request_ip+Date.today.to_s,Time.now)
    end
  end

end
