class Device < ActiveRecord::Base
  attr_accessible :group_id, :hostname, :description, :rtx

  belongs_to :group, :class_name=>"DeviceGroup", :foreign_key=>"group_id"
  has_many :alerts

  def rrd_fetch()
    filename = "#{ENV['rrd_file_path']}#{self.hostname}.rrd"
    if File.exists?(filename)
      fetch_data=`#{ENV['rrd_path']} fetch #{filename} AVERAGE -s now-10minute`
      return fetch_data.split("\n")[2].split()
    end

    return []
  end

  def self.get_counts(stime,etime)
    if stime.nil? and etime.nil?
      stime = Date.today.yesterday.to_s
      etime = Date.today.to_s
    end
    counts = Array.new
    Device.all.each do |d|
      a = Hash.new
      a["devname"] = d.hostname
      a["description"] = d.description
      a["count"]   = d.alerts.where(["created_at>? and created_at<?",Time.parse(stime),Time.parse(etime)]).count
      counts << a
    end
    return counts.sort_by{|a| a["count"]}.reverse
  end

  def self.sync
    uri = URI.parse('http://rcmsapi.chinacache.com:36000/devicemrtgs')
    http = Net::HTTP.new(uri.host, uri.port)
    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)
    devices = JSON.parse(response.body)

    devices.each do |dev|
      device = Device.where(["binary hostname=?", dev['devName']]).first
      if device
        device.update_attributes(description:dev["description"])
      end
    end
  end
end
