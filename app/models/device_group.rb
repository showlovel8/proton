class DeviceGroup < ActiveRecord::Base
  attr_accessible :name

  has_many :devices,:foreign_key=>"group_id"
end
