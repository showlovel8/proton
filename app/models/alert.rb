class Alert < ActiveRecord::Base
  attr_accessible :device_id, :ms
  belongs_to :device

  class << self

    def get_one_day_info(stime, etime)
      p stime
      p etime
      if stime.nil? and etime.nil?
        stime = Date.today.yesterday.to_s
        etime = Date.today.to_s
      end

      alerts = Alert.where(["created_at>? and created_at<?",Time.parse(stime),Time.parse(etime)])

      info ||= Hash.new
      alerts.each do |alert|
        time = alert.created_at
        hour = time.hour
        min  = time.min
        time_at = hour*12 + min/5
        if info["#{time_at*5/60}:#{time_at*5%60}"].nil?
          info["#{time_at*5/60}:#{time_at*5%60}"] = 0
        end
        info["#{time_at*5/60}:#{time_at*5%60}"] += 1
      end

      return info

    end 

  end

end
