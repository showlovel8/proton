#encoding: utf-8
class Note
  def self.create_rrd(filename)
    cmd = "#{ENV['rrd_path']} create #{filename}
      --step 300
      DS:hr:GAUGE:600:0:12500000000000
      DS:os:GAUGE:600:0:12500000000000
      DS:rt:GAUGE:600:0:12500000000000
      DS:rs:COUNTER:600:0:12500000000000
      DS:co:GAUGE:600:0:12500000000000
      DS:vs:GAUGE:600:0:12500000000000
      RRA:AVERAGE:0.5:1:600
      RRA:AVERAGE:0.5:4:600
      RRA:AVERAGE:0.5:24:600
      RRA:AVERAGE:0.5:288:730"

    system cmd.split("\n").join()
  end

  def self.update_rrd(filename,notes)
    velocity = 0
    unless notes['mean_velocity'].nil?
      velocity = notes['mean_velocity'].to_f
    end
    cmd="#{ENV['rrd_path']} updatev #{filename} N:#{notes['hit_rate'].to_f}:#{notes['cache_obj'].to_i}:#{notes['response_time'].to_f}:#{notes['http_requests'].to_i}:#{notes['cpu_usage'].to_f}:#{velocity}"
    system cmd
  end

  def self.update_group_rrd(filename,notes)
    if not File.exists?(filename)
      cmd = "#{ENV['rrd_path']} create #{filename}
        --step 300
        DS:hrl70:GAUGE:600:0:12500000000000
        DS:hrl90:GAUGE:600:0:12500000000000
        DS:hrm90:GAUGE:600:0:12500000000000
        DS:rtl2:GAUGE:600:0:12500000000000
        DS:rtl5:GAUGE:600:0:12500000000000
        DS:rtm5:GAUGE:600:0:12500000000000
        RRA:AVERAGE:0.5:1:600
        RRA:AVERAGE:0.5:4:600
        RRA:AVERAGE:0.5:24:600
        RRA:AVERAGE:0.5:288:730"

      system cmd.split("\n").join()
    end
    cmd="#{ENV['rrd_path']} updatev #{filename} N:#{notes['hostname']}:#{notes['hit_rate']}:#{notes['cache_obj']}:#{notes['response_time']}:#{notes['http_requests']}:#{notes['cpu_usage']}"
    system cmd
  end

  def self.graph(hostname,ds,multiple,vname,label,accuracy,upper_limit)
    cmd = "env LANG=C #{ENV['rrd_path']} graph  #{ENV['rrd_png_path']}#{hostname}-#{ds}.png --start now-120000
     -w 500 -h 120
     -t \"#{hostname} - #{label}\"
     -v \"#{vname}\"
     
     DEF:value1=/data/proton/rrds/#{hostname}.rrd:#{ds}:AVERAGE
     CDEF:value2=value1,#{multiple},*
     
     LINE1:value2#0000FF:\"#{label}\"
     GPRINT:value2:MAX:\"Maximum\\:%6.#{accuracy}lf\"
     GPRINT:value2:MIN:\"Minimum\\:%6.#{accuracy}lf\"
     GPRINT:value2:LAST:\"Current\\:%6.#{accuracy}lf\"

     COMMENT:\"LAST UPDATED \\:$(date '+%Y-%m-%d %H\\:%M')\n\" -Y
    "
    if upper_limit
      cmd = cmd + " --upper-limit #{upper_limit} --rigid"
    end
    system cmd.split("\n").join()
  end

  def self.graph_bps(hostname,ds,multiple,vname,label,accuracy,upper_limit)
    cmd = "env LANG=C #{ENV['rrd_path']} graph  #{ENV['rrd_png_path']}#{hostname}-#{ds}.png --start now-120000
     -w 500 -h 120
     -t \"#{hostname} - #{label}\"
     -v \"#{vname}\"
     
     DEF:value1=/data/proton/rrds/#{hostname}.rrd:#{ds}:AVERAGE
     CDEF:value2=value1,#{multiple},*
     
     LINE1:value2#0000FF:\"#{label}\"
     GPRINT:value2:MAX:\"Maximum\\:%6.#{accuracy}lf %SBps\"
     GPRINT:value2:MIN:\"Minimum\\:%6.#{accuracy}lf %SBps\"
     GPRINT:value2:LAST:\"Current\\:%6.#{accuracy}lf %SBps\"
     GPRINT:value2:AVERAGE:\"Average\\:%6.#{accuracy}lf %SBps\"

     COMMENT:\"LAST UPDATED \\:$(date '+%Y-%m-%d %H\\:%M')\n\" -Y
    "
    if upper_limit
      cmd = cmd + " --upper-limit #{upper_limit} --rigid"
    end
    system cmd.split("\n").join()
  end

  def self.graph_group(groupname,png_name,ds,label,vname)
    cmd = "env LANG=C #{ENV['rrd_path']} graph #{ENV['rrd_png_path']}#{groupname}-#{png_name}.png --start now-120000
     -w 500 -h 120
     -t \"#{groupname} - Hit Rate\"
     -v \"#{vname}\"
     
     DEF:value1=#{ENV['rrd_file_path']}#{groupname}.rrd:#{ds[0]}:AVERAGE
     DEF:value2=#{ENV['rrd_file_path']}#{groupname}.rrd:#{ds[1]}:AVERAGE
     DEF:value3=#{ENV['rrd_file_path']}#{groupname}.rrd:#{ds[2]}:AVERAGE
     
     AREA:value1#0000FF:\"#{label[0]}\":STACK
     GPRINT:value1:LAST:\"%6.0lf\"

     AREA:value2#00FF00:\"#{label[1]}\":STACK
     GPRINT:value2:LAST:\"%6.0lf\"

     AREA:value3#FF0000:\"#{label[2]}\":STACK
     GPRINT:value3:LAST:\"%6.0lf\"

     COMMENT:\"LAST UPDATED \\:$(date '+%Y-%m-%d %H\\:%M')\n\" -Y
    "
    system cmd.split("\n").join()
  end
end
