#encoding: utf-8
class Rtx
  def self.send_notify(message)
    msgs = message.scan(/.{1,460}/)
    msgs.each do |msg|
      Receiver.all.each do |receiver|
        Net::HTTP.start("rtx.chinacache.com", 8012) do |http|
          query={msg:"#{Time.now.to_formatted_s(:db)} #{msg}".encode(Encoding.find("GBK"),Encoding.find("UTF-8")),receiver:receiver.name.encode(Encoding.find("GBK"),Encoding.find("UTF-8"))}.to_query
          response = http.post("/sendnotify.cgi?",query)
          puts response.body
        end
      end
    end
  end
end
