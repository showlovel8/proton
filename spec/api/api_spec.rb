require 'spec_helper'

describe Proton::API do
   describe "Get /alert/count.json" do
      it "should be ok" do
        get "/alert/count.json"
        response.status.should == 200
      end
   end

   describe "Get /alert/items.json" do
      it "should be ok" do
        get "/alert/items.json"
        response.status.should == 200
      end
   end
end
