require "spec_helper"

describe FiverrorsController do
  describe "routing" do

    it "routes to #index" do
      get("/fiverrors").should route_to("fiverrors#index")
    end

    it "routes to #new" do
      get("/fiverrors/new").should route_to("fiverrors#new")
    end

    it "routes to #show" do
      get("/fiverrors/1").should route_to("fiverrors#show", :id => "1")
    end

    it "routes to #edit" do
      get("/fiverrors/1/edit").should route_to("fiverrors#edit", :id => "1")
    end

    it "routes to #create" do
      post("/fiverrors").should route_to("fiverrors#create")
    end

    it "routes to #update" do
      put("/fiverrors/1").should route_to("fiverrors#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/fiverrors/1").should route_to("fiverrors#destroy", :id => "1")
    end

  end
end
