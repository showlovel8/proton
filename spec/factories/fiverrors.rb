# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :fiverror do
    ip "MyString"
    count 1
  end
end
