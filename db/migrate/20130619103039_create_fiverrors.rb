class CreateFiverrors < ActiveRecord::Migration
  def change
    create_table :fiverrors do |t|
      t.string :ip
      t.integer :count

      t.timestamps
    end
  end
end
