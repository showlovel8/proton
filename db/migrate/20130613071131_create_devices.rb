class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :hostname
      t.integer :group_id
      t.string :description

      t.timestamps
    end
  end
end
