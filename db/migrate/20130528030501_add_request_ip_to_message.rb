class AddRequestIpToMessage < ActiveRecord::Migration
  def change
    add_column :messages, :request_ip, :string
    add_column :messages, :update_time, :string
  end
end
