class AddHostNameToFiverror < ActiveRecord::Migration
  def change
    add_column :fiverrors, :hostname, :string
    add_column :fiverrors, :area, :string
  end
end
