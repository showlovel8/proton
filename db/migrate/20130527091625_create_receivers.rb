#encoding: utf-8
class CreateReceivers < ActiveRecord::Migration
  def change
    create_table :receivers do |t|
      t.string :name

      t.timestamps
    end
    puts 'SETTING UP DEFAULT RECEIVER yachuan'
    Receiver.create! :name => '侯石'
    puts 'SETTING UP DEFAULT RECEIVER houshi'
    Receiver.create! :name => '陈亚川'
  end
end
