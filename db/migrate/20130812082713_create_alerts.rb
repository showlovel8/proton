class CreateAlerts < ActiveRecord::Migration
  def change
    create_table :alerts do |t|
      t.float :ms
      t.integer :device_id

      t.timestamps
    end
  end
end
