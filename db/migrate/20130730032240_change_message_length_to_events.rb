class ChangeMessageLengthToEvents < ActiveRecord::Migration
  def change
    change_column :events,:message, :string, :limit => 10000
  end
end
