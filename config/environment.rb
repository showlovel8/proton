# Load the rails application
require File.expand_path('../application', __FILE__)

# Initialize the rails application
Analysislog::Application.initialize!

$status_log = Logger.new("#{Rails.root}/log/status.log", "daily")
$git_log    = Logger.new("#{Rails.root}/log/git.log")
