require 'sidekiq/web'
require 'api'

Analysislog::Application.routes.draw do
  resources :fiverrors


  resources :receivers
  resources :devices

  resources :events,:only=>[:index,:destroy]


  resources :messages,:only=>[:index,:create]
  match '/notes' => 'messages#note', :as => "notes", :via => :post
  match '/status' => 'messages#status', :as => "status", :via => :post

  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#index"

  devise_for :users
  resources :users

  constraint = lambda { |request| request.env['warden'].authenticate!({ scope: :user }) }
  constraints constraint do
    mount Sidekiq::Web => '/sidekiq'
  end

  mount Proton::API => '/'
end
